let images = [
    'i.png','v.png', 'f.png',
]

let num = 0;

function forwImg() {
    let slider = document.getElementById('slider');
    num++;
    
    if (num >= images.length) {
        num = 0;
    }
    slider.src = images[num];
}

function backImg() {
    let slider = document.getElementById('slider');
    num--;
    
    if (num < 0) {
        num = images.length - 1;
    }
    slider.src = images[num];

}


function handleClick() {
    const inputValue = document.getElementById("text").value;
    if (inputValue === '') {
        alert("Вы должны что-то написать!");
      } else {

    
    const li = document.createElement("li");
    const div = document.createElement("div");
    const deleteBtn = document.createElement("button");
    deleteBtn.innerText = "Удалить"
    li.appendChild(div);
    li.appendChild(deleteBtn);

    div.innerText = inputValue;
    const ul = document.getElementById("list");
    ul.appendChild(li);
    deleteBtn.addEventListener("click", function(){
        li.remove()
    })
    const changeBtn = document.createElement("button");
    changeBtn.innerText = "Изменить"
    li.appendChild(changeBtn);
    changeBtn.addEventListener("click",function(){
        const input = document.createElement("input");
        li.replaceChild(input, div);
    });
}    
}

let box = document.getElementById('box');

 function test() {
  box.classList.add('box_animate_2');
}


    

// Отслеживаем окончание анимации
box.addEventListener("animationend", AnimationHandler, false);

function AnimationHandler () {
  // Удаляем класс с анимацией
  box.classList.remove('box_animate_2');
  
}

